import { __, inc, curry, not, has, compose } from 'ramda';
import { combineReducers } from 'redux';

const count = (state = 0, action) => {
  if ( action.type !== 'a' ) {
    return state;
  }
  return inc(state);
};

const createReducers = reds => {
  return combineReducers({
    count,
    ...reds,
  });
};

const addPlayerReducers = ( store, playerName, allReducers ) => {
  store.reducers[playerName] = combineReducers(allReducers);
  store.replaceReducer( createReducers( store.reducers ) );
};

export default curry( ( allReducers, { store }, _, next, action ) => {
  const isPlayerExists = has(__, store.getState());
  const isPlayerNotExists = compose( not, isPlayerExists );
  if ( isPlayerNotExists(action.player) ) {
    addPlayerReducers(store, action.player, allReducers);
  }
  return next(action);
});

