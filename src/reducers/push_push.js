import { compose, defaultTo, inc, over, lensProp, propSatisfies, equals } from 'ramda';

// const expectedTypes = [ 'GITLAB_PUSH', 'DISABLE_PUSH_PUSH' ];

export const winFunction = propSatisfies( equals(3), 'pushEvents' );

const initialState = {
  pushEvents: 0,
};


export const pushPush = ( state = initialState, action ) => {

  switch ( action.type ) {
    case 'GITLAB_PUSH':
      return over( lensProp('pushEvents'), compose( inc, defaultTo(0) ), state );
    default: return state;
  }
};


