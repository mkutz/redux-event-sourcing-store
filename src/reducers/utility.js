import { curry, compose, set, prop, not, allPass, propEq, or, over, lensProp, F, T } from 'ramda';

const nor = compose( not, or );
const reducerIsActive = allPass([
  propEq('enabled', true),
  propEq('finished', false),
]);

const relevantPlayer = propEq( 'player' );

const achievementFinishedAction = player => ( {
  meta: {
    delay: 5,
  },
  type: 'ACHIEVEMENT_FINISHED',
  payload: {
    achievement: 'push_push',
    player,
  },
} );

const initialGenericState = {
  enabled: true,
  finished: false,
  data: {},
};

const ID = String;
// state :: [ Object { enabled :: boolean, finished :: boolean, data: [ Object {...} ] } ]
// action :: [ Object { player :: ID, type :: String, data :: [ Object {...} ] } ]

export default curry( ( {
  reducer,
  winFunction,
}, player, state = initialGenericState, action ) => {
  return dispatch => {
    const delayedDispatchCompleted = dispatch.bind(this, achievementFinishedAction );

    switch ( action.type ) {
      case 'DISABLE_PUSH_PUSH':
        return over( lensProp('enabled'), F, state );
      case 'ENABLE_PUSH_PUSH':
        return over( lensProp('enabled'), T, state );
    }

    if ( nor( relevantPlayer(player, action), reducerIsActive(state) ) ) {
      return state;
    }

    const next = reducer( state ? prop('data', state) : undefined, action);

    if ( winFunction(next) ) {
      delayedDispatchCompleted('yay');
      return compose( over( lensProp('finished'), T ), set( lensProp('data'), next ) )(state);
    }

    return set( lensProp('data'), next, state );
  };
} );

