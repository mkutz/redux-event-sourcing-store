import middleware from '../../src/middleware/checkUser.js';
import { compose, ifElse, propEq, inc, defaultTo, add, product, identity } from 'ramda';
import { applyMiddleware, createStore, combineReducers } from 'redux';
import expect from 'expect';

const accumulate = (state = 0, action) => {
  if ( action.type !== 'a' )
    return state;
  return add(state, action.data);
};
const prod = (state = 1, action) => {
  if ( action.type !== 'a' )
    return state;
  return product([ state, action.data ]);
};
const count = (state = 0, action) => {
  if ( action.type !== 'a' ) {
    return state;
  }
  return inc(state);
};

const generateStore = () => {
  const temp = {};
  const store = createStore(
    combineReducers({ count }),
    {},
    applyMiddleware(
      middleware({accumulate, prod}, temp)
    )
  );
  temp.store = store;
  store.reducers = {};
  return store;
};

const store = generateStore();


const actionWithPlayer1 = {
  player: 'kaka',
  data: 2,
  type: 'a',
};

const actionWithPlayer2 = {
  player: 'kaki',
  type: 'a',
  data: 3,
};

(() => {
  let expectedState = {
    count: 1,
    kaka: {
      accumulate: 2,
      prod: 2,
    },
  };
  store.dispatch(actionWithPlayer1);
  expect(store.getState()).toEqual(expectedState);
  expectedState = {
    count: 2,
    kaka: {
      accumulate: 4,
      prod: 4,
    },
  };
  store.dispatch(actionWithPlayer2);
  expectedState = {
    count: 3,
    kaka: {
      accumulate: 7,
      prod: 12,
    },
    kaki: {
      accumulate: 3,
      prod: 3,
    },
  };
})();
