import {} from 'ramda';
import expect from 'expect';
import { pushPush as reducer, winFunction } from '../../src/reducers/push_push.js';

const pushAction = {
  type: 'GITLAB_PUSH',
};

const notPushAction = {
  type: 'something else',
};

( () => {
  let expectedValue = {
    pushEvents: 1,
  };
  let result = {};
  expect( result = reducer( undefined, pushAction ) ).toEqual( expectedValue );
  expect( winFunction(result) ).toEqual(false);
  expectedValue = {
    pushEvents: 2,
  };
  expect( result = reducer( result, pushAction ) ).toEqual( expectedValue );
  expect( result = reducer( result, notPushAction ) ).toEqual( expectedValue );
  expect( result = reducer( result, notPushAction ) ).toEqual( expectedValue );
  expect( winFunction(result) ).toEqual(false);
  expectedValue = {
    pushEvents: 3,
  };
  expect( result = reducer( result, pushAction ) ).toEqual( expectedValue );
  expect( winFunction(result) ).toEqual(true);

} )();
console.log('Testing push_push is ok =]');
