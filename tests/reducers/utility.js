import expect from 'expect';
import { identity } from 'ramda';
import kakiReducer from '../../src/reducers/utility.js';
import { pushPush as reducer, winFunction } from '../../src/reducers/push_push.js';

const dispatch = identity;
const call = ( result, action ) => kakiReducer({ player: 'michael', reducer, winFunction }, result, action )(dispatch);

(() => {
  let expectedState = {
    enabled: true,
    finished: false,
    data: {
      pushEvents: 1,
    },
  };
  const pushAction = {
    type: 'GITLAB_PUSH',
  };
  const disableAction = {
    type: 'DISABLE_PUSH_PUSH',
  };
  const enableAction = {
    type: 'ENABLE_PUSH_PUSH',
  };

  let result = {};
  expect( result = call( undefined, pushAction ) ).toEqual( expectedState );

  expectedState = {
    enabled: false,
    finished: false,
    data: {
      pushEvents: 1,
    },
  };
  expect( result = call( result, disableAction )).toEqual( expectedState );
  expect( result = call( result, pushAction ) ).toEqual( expectedState );

  expectedState = {
    enabled: true,
    finished: false,
    data: {
      pushEvents: 1,
    },
  };

  expect( result = call( result, enableAction ) ).toEqual( expectedState );
  expectedState = {
    enabled: true,
    finished: false,
    data: {
      pushEvents: 2,
    },
  };
  expect( result = call( result, pushAction ) ).toEqual( expectedState );
  expectedState = {
    enabled: true,
    finished: true,
    data: {
      pushEvents: 3,
    },
  };
  expect( result = call( result, pushAction ) ).toEqual( expectedState );
})();

console.log('Tests completed successfully!');
